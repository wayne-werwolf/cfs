
#=============================================================================
# Set global variables, configured from the external build.
#=============================================================================
SET(CFS_SOURCE_DIR "@CFS_SOURCE_DIR@")
SET(WIN32 "@WIN32@")

#=============================================================================
# Include some convenient macros (e.g. for applying patches).
#=============================================================================
INCLUDE("${CFS_SOURCE_DIR}/cmake_modules/CFS_macros.cmake")

#=============================================================================
# Apply some patches.
#=============================================================================
set(patches "flann-for-cfs.patch")

list(APPEND patches
  # remove unused variable in header code which fails for strict debug with clang
  "flann-unused-p1.patch" 
 
  # Extra commas at the end of enum lists may irritate some compilers.
  "flann-extra-commas-at-end-of-enum-lists.patch"

  # warning: ‘struct flann::anyimpl::base_any_policy’ has virtual functions
  # but non-virtual destructor 
  "flann-any.patch")

# both cmake patches are almost the same and possibly could be merged?!  
if(UNIX AND CMAKE_VERSION VERSION_GREATER "3.11")
  # needed due to https://github.com/mariusmuja/flann/issues/369
  list(APPEND patches "flann-linux-gcc-cmakev3.11-plus.patch")
elseif()
  # flann will not build with cmake version > 3.10
  # https://github.com/mariusmuja/flann/issues/369
  # this is a tricky patch untill flann is fixed - there is 1.9.2 but this does not work on Windows out of the box 
  list(APPEND patches "flann-new-cmake.patch")
endif()

APPLY_PATCHES("${patches}" "${CFS_SOURCE_DIR}/cfsdeps/flann")
